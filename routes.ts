import {
  buscarAction,
} from "./src/controller/BuscarAction";

/**
 * All application routes.
 */
export const AppRoutes = [
  {
    path: "/buscar",
    method: "get",
    action: buscarAction,
  }
];
