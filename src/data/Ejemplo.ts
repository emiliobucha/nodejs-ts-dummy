import { Column, Entity } from "typeorm";

@Entity("ejemplo", { schema: "esquema-test" })
export class Ejemplo {
  @Column("int", { primary: true, name: "id" })
  id: number;

  @Column("varchar", { name: "nombre", nullable: true, length: 45 })
  nombre: string | null;
}
