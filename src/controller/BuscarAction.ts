import { Request, Response } from "express";
import { BuscarService } from "../services/buscar.service";

export async function buscarAction(
  request: Request,
  response: Response
) {
  let buscarService = new BuscarService();
  buscarService.buscar(request.body).then((x) => {
    response.send(x);
  });
}
