import { getManager } from "typeorm";
import { Ejemplo } from "../data/Ejemplo";


export class BuscarService {
  constructor() {}
  async buscar(body: any) {
    try {
      const ejemploRepository = await getManager().getRepository(Ejemplo);
      const p = await ejemploRepository.find();
      return p;
    } catch (e) {
      return {};
    }
  }
}
