import { createConnection, getConnectionOptions } from "typeorm";
export const connectDB = async () => {
  await createConnection();
};
