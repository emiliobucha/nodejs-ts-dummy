import "reflect-metadata";
import "dotenv/config";
import { Request, Response } from "express";
import express from "express";
import * as bodyParser from "body-parser";
import { AppRoutes } from "./routes";
import { connectDB } from "./database";

const app = express();
app.use(bodyParser.json());
process.env.TZ = "America/Argentina/Cordoba";

AppRoutes.forEach((route) => {
  app.use(
    route.path,
    (request: Request, response: Response, next: Function) => {
      route
        .action(request, response)
        .then(() => next)
        .catch((err: any) => next(err));
    }
  );
});


const startServer = async () => {
  await app.listen(process.env.PORT || 8080, () => {
    console.log(`
  Server running on http://127.0.0.1:${process.env.PORT}
  `);
  });
};

(async () => {
  await connectDB();
  await startServer();
})();
