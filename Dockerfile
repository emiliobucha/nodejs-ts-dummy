FROM node:erbium-alpine

RUN npm install pm2 -g

RUN adduser node root
WORKDIR /home/node/app
ENV PM2_HOME=/home/node/app/.pm2
COPY package.json ./package.json
RUN npm install
COPY . ./
COPY ormconfig-prod.json ./ormconfig.json

RUN npm run build

RUN chmod -R 775 /home/node/app
RUN chown -R node:root /home/node/app

EXPOSE 3102

USER 1000
CMD [ "pm2-runtime", "npm", "--", "start" ]